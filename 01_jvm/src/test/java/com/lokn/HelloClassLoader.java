package com.lokn;

import java.util.Base64;

/**
 * @description:
 * @author: lokn
 * @date: 2022/03/01 23:23
 */
public class HelloClassLoader extends ClassLoader {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        new HelloClassLoader().findClass("com.lokn.HelloTest").newInstance();
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        String base = "yv66vgAAADQAIgoABgAUCQAVABYIABcKABgAGQcAGgcAGwEABjxpbml0PgEAAygpVgEABENvZGUBAA9MaW5lTnVtYmVyVGFibGUBABJMb2NhbFZhcmlhYmxlVGFibGUBAAR0aGlzAQAUTGNvbS9sb2tuL0hlbGxvVGVzdDsBAARtYWluAQAWKFtMamF2YS9sYW5nL1N0cmluZzspVgEABGFyZ3MBABNbTGphdmEvbGFuZy9TdHJpbmc7AQAKU291cmNlRmlsZQEADkhlbGxvVGVzdC5qYXZhDAAHAAgHABwMAB0AHgEAEmhlbGxvIGNsYXNzIGxvYWRlcgcAHwwAIAAhAQASY29tL2xva24vSGVsbG9UZXN0AQAQamF2YS9sYW5nL09iamVjdAEAEGphdmEvbGFuZy9TeXN0ZW0BAANvdXQBABVMamF2YS9pby9QcmludFN0cmVhbTsBABNqYXZhL2lvL1ByaW50U3RyZWFtAQAHcHJpbnRsbgEAFShMamF2YS9sYW5nL1N0cmluZzspVgAhAAUABgAAAAAAAgABAAcACAABAAkAAAAvAAEAAQAAAAUqtwABsQAAAAIACgAAAAYAAQAAAAgACwAAAAwAAQAAAAUADAANAAAACQAOAA8AAQAJAAAANwACAAEAAAAJsgACEgO2AASxAAAAAgAKAAAACgACAAAACwAIAAwACwAAAAwAAQAAAAkAEAARAAAAAQASAAAAAgAT";
        byte[] decode = decode(base);
        return defineClass(name, decode, 0, decode.length);
    }

    public static byte[] decode(String base64Str) {
        byte[] decode = Base64.getDecoder().decode(base64Str);
        return decode;
    }

}
