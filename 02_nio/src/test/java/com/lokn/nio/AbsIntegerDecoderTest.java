package com.lokn.nio;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @description:
 * @author: lokn
 * @date: 2022/04/19 23:23
 */
public class AbsIntegerDecoderTest {

    @Test
    public void testAbsInteger() {
        ByteBuf buffer = Unpooled.buffer();
        for (int i = 0; i < 10; i++) {
            buffer.writeInt(i * -1);
        }
        EmbeddedChannel channel = new EmbeddedChannel(new AbsIntegerDecoder());
        assertTrue(channel.writeInbound(buffer));
        assertTrue(channel.finish());

//        for (int i = 0; i < 10; i++) {
//            assertEquals(i, channel.readOutbound());
//        }
        assertNull(channel.readOutbound());

    }

}
