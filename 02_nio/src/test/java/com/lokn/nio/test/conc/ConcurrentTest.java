package com.lokn.nio.test.conc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/19 23:13
 */
public class ConcurrentTest {

    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(5, new Runnable() {
            @Override
            public void run() {
                System.out.println("这是回调函数，回调线程的为：" + Thread.currentThread().getName());
            }
        });
        for (int i = 0; i < 5; i++) {
            new CyclicBarrierTask(cyclicBarrier, i).start();
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("main 主线程处理完成！");
    }

    static class CyclicBarrierTask extends Thread {

        private CyclicBarrier cyclicBarrier;
        private int id;

        public CyclicBarrierTask(CyclicBarrier cyclicBarrier, int id) {
            this.cyclicBarrier = cyclicBarrier;
            this.id = id;
        }

        @Override
        public void run() {
            System.out.println(new String().format("线程【%s】我是第【%d】条数据，开始进行CyclicBarrier....",
                    Thread.currentThread().getName(), id));
            try {
                Thread.sleep(1000);
                cyclicBarrier.await();
                System.out.println(new String().format("线程【%s】我是第【%d】条数据，CyclicBarrier 任务处理完毕！",
                        Thread.currentThread().getName(), id));
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }

}
