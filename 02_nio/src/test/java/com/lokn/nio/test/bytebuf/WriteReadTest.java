package com.lokn.nio.test.bytebuf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.CompositeByteBuf;
import io.netty.buffer.Unpooled;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/04 16:45
 */
public class WriteReadTest {

    static final Charset UTF_8 = Charset.forName("UTF-8");

    @Test
    public void testWriteRead() {
        ByteBuf buffer = ByteBufAllocator.DEFAULT.buffer(9, 100);
        System.out.println("动作：分配ByteBuf(9, 100)" + buffer);
        buffer.writeBytes(new byte[] {1, 2, 3, 4});
        System.out.println("写入：写入4个字节，new byte(){1, 2, 3, 4}");
        System.out.println("start============: get ===============");
        getByteBuf(buffer);
        System.out.println("end============: get ===============");
        System.out.println("start============: read ===============");
        readByteBuf(buffer);
        System.out.println("start============: read ===============");
    }

    private void readByteBuf(ByteBuf byteBuf) {
        while (byteBuf.isReadable()) {
            System.out.println("获取一个字节：" + byteBuf.readByte());
        }
    }

    private void getByteBuf(ByteBuf byteBuf) {
        for (int i = 0; i < byteBuf.readableBytes(); i++) {
            System.out.println("读取一个字节：" + byteBuf.getByte(i));
        }
    }

    @Test
    public void heapByteBuf() {
        ByteBuf buffer = ByteBufAllocator.DEFAULT.heapBuffer();
        buffer.writeBytes("这是heapByteBuf".getBytes(StandardCharsets.UTF_8));
        System.out.println(buffer.hasArray());
        if (buffer.hasArray()) {
            byte[] array = buffer.array();
            int index = buffer.arrayOffset() + buffer.readerIndex();
//            int index = 0;
            int length = buffer.readableBytes();
            System.out.println(new String(array, index, length, UTF_8));
        }
        buffer.release();
    }

    @Test
    public void directByteBuf() {
        ByteBuf byteBuf = ByteBufAllocator.DEFAULT.directBuffer();
        byteBuf.writeBytes("这个是directByteBuf".getBytes(StandardCharsets.UTF_8));
        System.out.println(byteBuf.hasArray());
        if (!byteBuf.hasArray()) {
            int length = byteBuf.readableBytes();
            byte[] bytes = new byte[length];
            byteBuf.getBytes(byteBuf.readerIndex(), bytes);
            System.out.println(new String(bytes));
        }
    }

    @Test
    public void compositeByte() {
        CompositeByteBuf byteBufs = ByteBufAllocator.DEFAULT.compositeBuffer();
        ByteBuf headerBuf = Unpooled.copiedBuffer("我是headerBuf".getBytes(StandardCharsets.UTF_8));
        ByteBuf bodyBuf = Unpooled.copiedBuffer("我是请求体".getBytes(StandardCharsets.UTF_8));

        byteBufs.addComponents(headerBuf, bodyBuf);
        sendMsg(byteBufs);
        headerBuf.retain();
        byteBufs.release();
        byteBufs = ByteBufAllocator.DEFAULT.compositeBuffer();
        bodyBuf = Unpooled.copiedBuffer("我是请求体2".getBytes(StandardCharsets.UTF_8));
        byteBufs.addComponents(headerBuf, bodyBuf);
        sendMsg(byteBufs);
        bodyBuf.release();
    }

    private void sendMsg(CompositeByteBuf byteBufs) {
        for (ByteBuf bf : byteBufs) {
            int length = bf.readableBytes();
            byte[] bytes = new byte[length];
            bf.getBytes(bf.readerIndex(), bytes);
            System.out.println(new String(bytes));
        }
    }

    @Test
    public void compositeBufferTest() {
        CompositeByteBuf byteBufs = Unpooled.compositeBuffer(3);
        byteBufs.addComponent(Unpooled.wrappedBuffer(new byte[]{1, 2}));
        byteBufs.addComponent(Unpooled.wrappedBuffer(new byte[]{3, 4}));
        byteBufs.addComponent(Unpooled.wrappedBuffer(new byte[]{5, 6}));

        ByteBuffer nioBuffer = byteBufs.nioBuffer(0,6);
        byte[] array = nioBuffer.array();
        for (byte b : array) {
            System.out.println(b);
        }
        byteBufs.release();
    }

}
