package com.lokn.nio.test;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/14 23:32
 */
public class EchoNettyServerHandler extends ChannelInboundHandlerAdapter {

    public static final EchoNettyServerHandler INSTANCE = new EchoNettyServerHandler();

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf byteBuf = (ByteBuf) msg;
        System.out.println("channelRead() : ByteBuf为=" + (byteBuf.hasArray() ? "堆内存" : "直接内存"));
        int length = byteBuf.readableBytes();
        byte[] bytes = new byte[length];
        byteBuf.getBytes(0, bytes);
        System.out.println("server received : " + new String(bytes));
        System.out.println("回前，msg.refCnt：" + ((ByteBuf) msg).refCnt());
        // 写回数据，异步任务
        ChannelFuture future = ctx.writeAndFlush(msg);
        future.addListener((ChannelFuture cf) -> {
            System.out.println("写回后，msg.refCnt：" + ((ByteBuf) msg).refCnt());
        });
    }
}
