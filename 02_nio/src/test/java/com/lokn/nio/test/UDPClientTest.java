package com.lokn.nio.test;

import org.apache.http.client.utils.DateUtils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Scanner;

/**
 * @description:
 * @author: lokn
 * @date: 2022/05/14 17:19
 */
public class UDPClientTest {

    public void updClient() throws IOException {
        DatagramChannel datagramChannel = DatagramChannel.open();
        datagramChannel.configureBlocking(false);
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        Scanner scanner = new Scanner(System.in);
        System.out.println("UDP client 启动。。。");
        System.out.println("请输入需要发送的消息：");
        while (scanner.hasNext()) {
            String msg = scanner.next();
            buffer.put((DateUtils.formatDate(new Date()) + " >>> "+ msg).getBytes(StandardCharsets.UTF_8));
            buffer.flip();
            datagramChannel.send(buffer, new InetSocketAddress("localhost", 8092));
            buffer.clear();
        }
        datagramChannel.close();
    }

}
