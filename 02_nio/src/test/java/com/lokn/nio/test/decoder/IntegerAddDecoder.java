package com.lokn.nio.test.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/19 10:48
 */
public class IntegerAddDecoder extends ReplayingDecoder<IntegerAddDecoder.Status> {

    enum Status {
        PARSE_1, PARSE_2;
    }

    private int first;
    private int second;

    public IntegerAddDecoder() {
        super(Status.PARSE_1);
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        switch (state()) {
            case PARSE_1:
                first = byteBuf.readInt();
                checkpoint(Status.PARSE_2);
                break;
            case PARSE_2:
                second = byteBuf.readInt();
                int sum = first + second;
                list.add(sum);
                checkpoint(Status.PARSE_1);
                break;
            default:
                break;
        }

    }

}
