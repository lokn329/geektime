package com.lokn.nio.test;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.embedded.EmbeddedChannel;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/03 23:54
 */
public class SimpleOutHandlerTest {

    static class OutHandlerA extends ChannelOutboundHandlerAdapter {
        @Override
        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
            System.out.println("出站处理器 A ： 被回调");
            super.write(ctx, msg, promise);
        }
    }

    static class OutHandlerB extends ChannelOutboundHandlerAdapter {
        @Override
        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
            System.out.println("出站处理器 B ： 被回调");
//            super.write(ctx, msg, promise);
        }
    }

    static class OutHandlerC extends ChannelOutboundHandlerAdapter {
        @Override
        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
            System.out.println("出站处理器 C ： 被回调");
            super.write(ctx, msg, promise);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        EmbeddedChannel i = new EmbeddedChannel(new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel ec) throws Exception {
                ChannelPipeline pipeline = ec.pipeline();
                pipeline.addLast(new OutHandlerA());
                pipeline.addLast(new OutHandlerB());
                pipeline.addLast(new OutHandlerC());
            }
        });

        ByteBuf buffer = Unpooled.buffer();
        buffer.writeInt(1);
        i.writeOutbound(buffer);
        i.flush();
        Thread.sleep(1000);
    }
}
