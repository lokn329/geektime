package com.lokn.nio.test;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.nio.charset.StandardCharsets;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/28 23:03
 */
public class EchoNettyDumpClient {

    private static final int port = 8990;

    public static void main(String[] args) {
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group);
            bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
            bootstrap.remoteAddress("localhost", port);
            bootstrap.channel(NioSocketChannel.class);
            Bootstrap handler = bootstrap.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    socketChannel.pipeline().addLast(new EchoNettyClientHandler());
                }
            });
            ChannelFuture connect = bootstrap.connect();
            connect.addListener((ChannelFuture cf) -> {
                if (cf.isSuccess()) {
                    System.out.println("EchoDumpClient 服务端连接成功！");
                } else {
                    System.out.println("EchoDumpClient 服务端连接失败！");
                }
            });
            ChannelFuture sync = connect.sync();
            Channel channel = connect.channel();
            String content= "疯狂创客圈:高性能学习者社群!";
            byte[] bytes = content.getBytes(StandardCharsets.UTF_8);
            for (int i = 0; i < 1000; i++) {
                ByteBuf buffer = channel.alloc().buffer();
                buffer.writeBytes(bytes);
                channel.writeAndFlush(buffer);
            }
            ChannelFuture future = channel.closeFuture();
            future.sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }
    }

}
