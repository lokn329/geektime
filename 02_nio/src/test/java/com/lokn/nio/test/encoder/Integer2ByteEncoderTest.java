package com.lokn.nio.test.encoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.Test;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/27 20:58
 */
public class Integer2ByteEncoderTest {

    @Test
    public void test1() throws InterruptedException {
        ChannelInitializer<EmbeddedChannel> channelInitializer = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel embeddedChannel) throws Exception {
                embeddedChannel.pipeline().addLast(new Integer2ByteEncoderHandler());
            }
        };
        EmbeddedChannel embeddedChannel = new EmbeddedChannel(channelInitializer);
        for (int i = 0; i < 10; i++) {
            embeddedChannel.write(i);
        }
        embeddedChannel.flush();
        ByteBuf o = embeddedChannel.readOutbound();
        while (o != null) {
            System.out.println(o.readInt());
            o = embeddedChannel.readOutbound();
        }

        Thread.sleep(1000);
    }

    @Test
    public void string2IntegerTest() throws InterruptedException {
        ChannelInitializer<EmbeddedChannel> channelInitializer = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel embeddedChannel) throws Exception {
                embeddedChannel.pipeline().addLast(new Integer2ByteEncoderHandler())
                        .addLast(new String2IntegerEncoderHandler());
            }
        };
        EmbeddedChannel embeddedChannel = new EmbeddedChannel(channelInitializer);
        for (int i = 0; i < 10; i++) {
            embeddedChannel.write(i + ", 我是StringToInteger");
        }
        embeddedChannel.flush();
        ByteBuf o = embeddedChannel.readOutbound();
        while (o != null) {
            System.out.println(o.readInt());
            o = embeddedChannel.readOutbound();
        }
        Thread.sleep(1000);
    }

}
