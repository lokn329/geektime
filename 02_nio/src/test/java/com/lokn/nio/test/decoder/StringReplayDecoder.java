package com.lokn.nio.test.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/19 14:24
 */
public class StringReplayDecoder extends ReplayingDecoder<StringReplayDecoder.State> {

    enum State {
        PARSE_01, PARSE_02;
    }

    public StringReplayDecoder() {
        super(State.PARSE_01);
    }

    int length;
    byte[] inByte;

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        switch (state())  {
            case PARSE_01:
                length = byteBuf.readInt();
                inByte = new byte[length];
                checkpoint(State.PARSE_02);
                break;
            case PARSE_02:
                byteBuf.readBytes(inByte, 0, length);
                list.add(new String(inByte, "UTF-8"));
                checkpoint(State.PARSE_01);
                break;
            default:
                break;
        }
    }
}
