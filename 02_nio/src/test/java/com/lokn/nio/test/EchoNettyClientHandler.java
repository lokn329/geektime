package com.lokn.nio.test;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/14 23:45
 */
public class EchoNettyClientHandler extends ChannelInboundHandlerAdapter {

    public static final EchoNettyClientHandler INSTANCE = new EchoNettyClientHandler();

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf byteBuf = (ByteBuf) msg;
        int length = byteBuf.readableBytes();
        byte[] bytes = new byte[length];
        byteBuf.getBytes(0, bytes);
        System.out.println("client received : " + new String(bytes));
        byteBuf.release();
    }
}
