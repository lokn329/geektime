package com.lokn.nio.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

/**
 * @description:
 * @author: lokn
 * @date: 2022/05/14 13:29
 */
public class SocketChannelTest {

    public void socketChannelTest1() throws IOException {
        String srcPath = "/Users/lokn/Desktop/test.txt";
        File file = new File(srcPath);
        FileChannel fileChannel = new FileInputStream(srcPath).getChannel();
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);
        ByteBuffer buffer = ByteBuffer.allocate(1024);

        try {
            socketChannel.connect(new InetSocketAddress("localhost", 8090));
            // 等待链接
            while (!socketChannel.finishConnect()) {}
            System.out.println("服务端连接成功。。。");
            Charset charset = Charset.forName("UTF-8");
            ByteBuffer fileNameBuffer = charset.encode("test");
            socketChannel.write(fileNameBuffer);
            ByteBuffer buffer1 = ByteBuffer.allocate(1024);
            buffer1.putLong(file.length());
            buffer1.flip();
            socketChannel.write(buffer1);
            System.out.println("开始传输文件。。。。");
            int length = 0;
            while ((length = fileChannel.read(buffer)) > 0) {
                buffer.flip();
                socketChannel.write(buffer);
                buffer.clear();
            }
            if (length == -1) {
                fileChannel.close();
                socketChannel.shutdownOutput();
                socketChannel.close();
            }
        } finally {

        }

    }

}
