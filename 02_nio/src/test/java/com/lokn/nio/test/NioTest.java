package com.lokn.nio.test;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.FileChannel;

/**
 * @description: 测试java NIO
 * @author: lokn
 * @date: 2022/05/12 23:32
 */
public class NioTest {


    @Test
    public void UseBufTest() {
        IntBuffer intBuffer = IntBuffer.allocate(20);
        for (int i = 0; i < 5; i++) {
            intBuffer.put(i);
        }
        intBuffer.flip();
        System.out.println("position = " + intBuffer.position());
        System.out.println("limit = " + intBuffer.limit());
        System.out.println("capacity = " + intBuffer.capacity());
        System.out.println("====================================");
        for (int i = 0; i < 2; i++) {
            System.out.println(intBuffer.get());
        }
        System.out.println("position = " + intBuffer.position());
        System.out.println("limit = " + intBuffer.limit());
        System.out.println("capacity = " + intBuffer.capacity());
        System.out.println("====================================");
        intBuffer.compact();
        System.out.println("position = " + intBuffer.position());
        System.out.println("limit = " + intBuffer.limit());
        System.out.println("capacity = " + intBuffer.capacity());
    }

    @Test
    public void fileChannel() {
        String rootPath = "/Users/lokn/Desktop/";
        FileInputStream fis = null;
        FileOutputStream fos = null;
        FileChannel readChannel = null;
        FileChannel writeChannel = null;
        try {
            fis = new FileInputStream(rootPath + "test.txt");
            fos = new FileOutputStream(rootPath + "test-copy.txt");
            readChannel = fis.getChannel();
            writeChannel = fos.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(100);
            int length = -1;
            while ((length = readChannel.read(buffer)) != -1) {
                buffer.flip();
                int len = 0;

                while ((len = writeChannel.write(buffer)) != 0) {
                    System.out.println("写入的字节数为：" + len);
                }
                buffer.clear();
            }
            writeChannel.force(true);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (writeChannel != null) {
                try {
                    writeChannel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
