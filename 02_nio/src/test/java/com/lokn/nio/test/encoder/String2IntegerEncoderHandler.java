package com.lokn.nio.test.encoder;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/27 23:23
 */
public class String2IntegerEncoderHandler extends MessageToMessageEncoder<String> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, String s, List<Object> list) throws Exception {
        char[] chars = s.toCharArray();
        for (char c : chars) {
            if (c >= 48 && c <= 58) {
                list.add(new Integer(c));
            }
        }

    }
}
