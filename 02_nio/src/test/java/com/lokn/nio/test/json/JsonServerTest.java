package com.lokn.nio.test.json;

import com.alibaba.fastjson.JSON;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;

/**
 * @description:
 * @author: lokn
 * @date: 2022/07/02 16:38
 */
public class JsonServerTest {

    public static void main(String[] args) throws InterruptedException {
        ChannelInitializer<EmbeddedChannel> channelInitializer = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel embeddedChannel) throws Exception {
                embeddedChannel.pipeline().addLast(new LengthFieldBasedFrameDecoder(1024, 0, 4, 0, 4));
                embeddedChannel.pipeline().addLast(new StringDecoder());
                embeddedChannel.pipeline().addLast(new JsonMsgDecoder());
            }
        };
        EmbeddedChannel embeddedChannel = new EmbeddedChannel(channelInitializer);
        for (int i = 0; i < 10; i++) {
            JsonMsg jsonMsg = new JsonMsg();
            jsonMsg.setId(i);
            jsonMsg.setContent(i + "->我是文本内容呀");
            Object o = JSON.toJSONString(jsonMsg);
            System.out.println(o);
            embeddedChannel.writeInbound(o);
        }
        embeddedChannel.flush();
        embeddedChannel.close().sync();
    }

}
