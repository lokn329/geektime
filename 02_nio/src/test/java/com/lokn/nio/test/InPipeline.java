package com.lokn.nio.test;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.embedded.EmbeddedChannel;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/03 17:38
 */
public class InPipeline {

    static class SimpleInPipelineA extends ChannelInboundHandlerAdapter {
        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            System.out.println("入站处理A，被回调。。。");
            super.channelRead(ctx, msg);
        }
    }

    static class SimpleInPipelineB extends ChannelInboundHandlerAdapter {
        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            System.out.println("入站处理B，被回调。。。");
//            super.channelRead(ctx, msg);
        }
    }

    static class SimpleInPipelineC extends ChannelInboundHandlerAdapter {
        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            System.out.println("入站处理C，被回调。。。");
            super.channelRead(ctx, msg);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        EmbeddedChannel channel = new EmbeddedChannel(new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel ec) throws Exception {
                ec.pipeline().addLast(new SimpleInPipelineA());
                ec.pipeline().addLast(new SimpleInPipelineB());
                ec.pipeline().addLast(new SimpleInPipelineC());
            }
        });
        ByteBuf buffer = Unpooled.buffer();
        buffer.writeInt(1);
        channel.writeInbound(buffer);
        channel.flush();
        Thread.sleep(1000);
    }

}
