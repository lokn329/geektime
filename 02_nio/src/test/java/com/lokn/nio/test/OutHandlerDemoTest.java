package com.lokn.nio.test;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.embedded.EmbeddedChannel;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/03 00:33
 */
public class OutHandlerDemoTest {

    public static void main(String[] args) throws InterruptedException {
        OutHandlerDemo out = new OutHandlerDemo();
        EmbeddedChannel embeddedChannel = new EmbeddedChannel(new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel embeddedChannel) throws Exception {
                embeddedChannel.pipeline().addLast(out);
            }
        });
        ByteBuf buffer = Unpooled.buffer();
        buffer.writeInt(1);
        embeddedChannel.writeOutbound(buffer);
//        embeddedChannel.flush();
        embeddedChannel.close();
        Thread.sleep(2000);
    }

}
