package com.lokn.nio.test;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.apache.http.client.utils.DateUtils;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Scanner;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/14 23:41
 */
public class EchoNettyClient {

    public static void main(String[] args) throws InterruptedException {
        runClient();
    }

    public static void runClient() throws InterruptedException {
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap bs = new Bootstrap();
            bs.group(group);
            bs.channel(NioSocketChannel.class);
            bs.remoteAddress("localhost", 8990);
            bs.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
            bs.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel sc) throws Exception {
                    sc.pipeline().addLast(EchoNettyClientHandler.INSTANCE);
                }
            });
            ChannelFuture connect = bs.connect();
            connect.addListener((ChannelFuture future) -> {
                if (future.isSuccess()) {
                    System.out.println("EchoClient 客户端链接成功！");
                } else {
                    System.out.println("EchoClient 客户端链接失败。");
                }
            });
            // 阻塞，直到连接成功
            connect.sync();
            Channel channel = connect.channel();
            Scanner scanner = new Scanner(System.in);
            System.out.println("请输入发送内容：");
            while (scanner.hasNext()) {
                String next = scanner.next();
                byte[] bytes = (DateUtils.formatDate(new Date()) + "=>" + next).getBytes(StandardCharsets.UTF_8);
                ByteBuf buffer = channel.alloc().buffer();
                buffer.writeBytes(bytes);
                channel.writeAndFlush(buffer);
                System.out.println("请输入发送内容：");
            }
        } finally {
            group.shutdownGracefully();
        }
    }

}
