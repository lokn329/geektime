package com.lokn.nio.test.decoder;

import cn.hutool.core.util.RandomUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/17 00:00
 */
public class DecoderDemo {

    @Test
    public void testDecoder() throws InterruptedException {
        ChannelInitializer<EmbeddedChannel> ci = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel ec) throws Exception {
                ec.pipeline().addLast(new Byte2IntegerDecoder())
                        .addLast(new Byte2IntegerDecoderHandler());
            }
        };
        EmbeddedChannel ec = new EmbeddedChannel(ci);
        for (int i = 0; i < 10; i++) {
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeInt(i);
            ec.writeInbound(buffer);
        }
        Thread.sleep(1000);
    }

    @Test
    public void integerAddDecoder() throws InterruptedException {
        ChannelInitializer<EmbeddedChannel> ci = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel ec) throws Exception {
                ec.pipeline().addLast(new IntegerAddDecoder())
                        .addLast(new IntegerAddDecoderHandler());
            }
        };

        EmbeddedChannel ec = new EmbeddedChannel(ci);

        for (int i = 0; i < 10; i++) {
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeInt(i);
            ec.writeInbound(buffer);
        }
        Thread.sleep(1000);

    }

    @Test
    public void stringDecoder() throws InterruptedException {
        ChannelInitializer<EmbeddedChannel> ci = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel ec) throws Exception {
                ec.pipeline().addLast(new StringReplayDecoder())
                        .addLast(new StringProcessHandler());
            }
        };
        String str = "我是一个字符串，我在进行Netty String Decoder 测试";
        byte[] bytes = str.getBytes(StandardCharsets.UTF_8);
        EmbeddedChannel ec = new EmbeddedChannel(ci);
        for (int i = 0;  i < 10; i++)  {
            int randomInt = RandomUtil.randomInt(1, 3);
            System.out.println("randomInt = " + randomInt);
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeInt(randomInt  * bytes.length);
            for (int j = 0; j < randomInt; j++) {
                buffer.writeBytes(bytes);
            }
            ec.writeInbound(buffer);
        }
        Thread.sleep(1000);
    }

    @Test
    public void stringDecoderByteToMsgDemo() throws InterruptedException {
        ChannelInitializer<EmbeddedChannel> ci = new ChannelInitializer<EmbeddedChannel>() {

            @Override
            protected void initChannel(EmbeddedChannel ec) throws Exception {
                ec.pipeline().addLast(new StringHeaderDecoder());
                ec.pipeline().addLast(new StringProcessHandler());
            }
        };

        EmbeddedChannel ec = new EmbeddedChannel(ci);

        String str = "我是测试，测试String Decoder！";
        byte[] bytes = str.getBytes(StandardCharsets.UTF_8);
        for (int i = 0; i < 10; i++) {
            int randomInt = RandomUtil.randomInt(1, 3);
            ByteBuf buffer = Unpooled.buffer();
            buffer.writeInt(bytes.length * randomInt);
            for (int j = 0; j < randomInt; j++) {
                buffer.writeBytes(bytes);
            }
            ec.writeInbound(buffer);
        }

        Thread.sleep(100);
    }

    @Test
    public void lineBaseFrameDecoderTest() {
        ChannelInitializer<EmbeddedChannel> ei = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel ec) throws Exception {
                ec.pipeline().addLast(new LineBasedFrameDecoder(1024))
                        .addLast(new StringDecoder())
                        .addLast(new StringProcessHandler());
            }
        };

        EmbeddedChannel ec = new EmbeddedChannel(ei);

        String msg = "我是测试，我要测试，我在测试lineBaseFrameDecoder！";
        String split = "\r\n";

        for (int i = 0; i < 10; i++) {
            ByteBuf buffer = Unpooled.buffer();
            int randomInt = RandomUtil.randomInt(1, 4);
            for (int j = 0; j < randomInt; j++) {
                buffer.writeBytes(msg.getBytes(StandardCharsets.UTF_8));
            }
            buffer.writeBytes(split.getBytes(StandardCharsets.UTF_8));
            ec.writeInbound(buffer);
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void lengthFiledBasedFrameDecoderTest() {
        String msg = "疯狂创客圈:高性能学习社群!";
        final LengthFieldBasedFrameDecoder d = new LengthFieldBasedFrameDecoder(1024, 0, 4, 2, 4);
        ChannelInitializer<EmbeddedChannel> ci = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel ec) throws Exception {
                ec.pipeline().addLast(d)
                        .addLast(new StringDecoder(StandardCharsets.UTF_8))
                        .addLast(new StringProcessHandler());
            }
        };
        EmbeddedChannel ec = new EmbeddedChannel(ci);
        for (int i = 1; i < 10; i++) {
            ByteBuf buffer = Unpooled.buffer();
            String str = i + "条" + msg;
            byte[] bytes = str.getBytes(StandardCharsets.UTF_8);
            buffer.writeInt(bytes.length);
            buffer.writeChar(100);
            buffer.writeBytes(bytes);
            ec.writeInbound(buffer);
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
