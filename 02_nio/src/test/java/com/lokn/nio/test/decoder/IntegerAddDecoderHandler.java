package com.lokn.nio.test.decoder;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/19 11:01
 */
public class IntegerAddDecoderHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        int i = (int) msg;
        System.out.println("channelRead = " + i);
    }
}
