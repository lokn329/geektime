package com.lokn.nio.test.json;

import lombok.Data;

/**
 * @description:
 * @author: lokn
 * @date: 2022/07/02 15:33
 */
@Data
public class JsonMsg {

    private Integer id;
    private String content;



}
