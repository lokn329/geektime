package com.lokn.nio.test;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.embedded.EmbeddedChannel;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/02 00:02
 */
public class InHandlerDemoTest {

    public static void main(String[] args) throws InterruptedException {
        InHandlerDemo inHandlerDemo = new InHandlerDemo();
        ChannelInitializer<EmbeddedChannel> init = new ChannelInitializer<EmbeddedChannel>() {
            @Override
            protected void initChannel(EmbeddedChannel embeddedChannel) throws Exception {
                embeddedChannel.pipeline().addLast(inHandlerDemo);
            }
        };
        EmbeddedChannel embeddedChannel = new EmbeddedChannel(init);
        ByteBuf buffer = Unpooled.buffer();
        buffer.writeInt(1);
        embeddedChannel.writeInbound(buffer);
        embeddedChannel.flush();
        embeddedChannel.close();
        Thread.sleep(2000);
    }

}
