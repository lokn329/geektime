package com.lokn.nio.test;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @description:
 * @author: lokn
 * @date: 2022/05/16 23:56
 */
public class SocketChannelServiceTest {

    private Charset charset = Charset.forName("UTF-8");

    private Map<SelectableChannel, Client> map = new HashMap<>();
    private ByteBuffer buffer = ByteBuffer.allocate(1024);

    static class Client {
        String fileName;
        Integer fileLength;
        InetSocketAddress socketAddress;
        FileChannel fileChannel;
    }

    public void startServer() throws IOException {
        Selector selector = Selector.open();
        // 服务端
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.bind(new InetSocketAddress(8099));
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        while (selector.select() > 0) {
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey next = iterator.next();
                if (next.isAcceptable()) {

                }
            }
        }
    }

}
