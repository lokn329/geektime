package com.lokn.nio.test.decoder;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/16 23:59
 */
public class Byte2IntegerDecoderHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        int i = (int) msg;
        System.out.println("channelRead 读取的值为：" + i);
    }
}
