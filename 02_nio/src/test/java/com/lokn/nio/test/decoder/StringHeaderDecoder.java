package com.lokn.nio.test.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/19 15:35
 */
public class StringHeaderDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        // 可读长度不够，则说明header未填满
        if (byteBuf.readableBytes() < 4) {
            return;
        }
        // 设置回滚点。回滚点为消息头的readIndex读指针的位置
        byteBuf.markReaderIndex();
        int length = byteBuf.readInt();
        if (byteBuf.readableBytes() < length) {
            byteBuf.resetReaderIndex();
            return;
        }
        byte[] bytes = new byte[length];
        byteBuf.readBytes(bytes, 0, length);
        list.add(new String(bytes, "UTF-8"));
    }

}
