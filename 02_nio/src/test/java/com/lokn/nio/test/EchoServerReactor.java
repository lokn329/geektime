package com.lokn.nio.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * @description:
 * @author: lokn
 * @date: 2022/05/19 20:39
 */
public class EchoServerReactor implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(EchoServerReactor.class);

    Selector selector;
    ServerSocketChannel socketChannel;

    public EchoServerReactor() throws IOException {
        selector = Selector.open();
        socketChannel = ServerSocketChannel.open();

        int port = 8090;
        socketChannel.socket().bind(new InetSocketAddress(port));
        socketChannel.configureBlocking(false);
        logger.info("服务端已经开始监听，监听端口为：{}", port);

        SelectionKey sk = socketChannel.register(selector, SelectionKey.OP_ACCEPT);
        sk.attach(new AcceptorHandler());
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                selector.select(1000);
                Set<SelectionKey> keySet = selector.selectedKeys();
                if (keySet == null || keySet.isEmpty()) {
                    continue;
                }
                Iterator<SelectionKey> iterator = keySet.iterator();
                while (iterator.hasNext()) {
                    SelectionKey next = iterator.next();
                    dispatch(next);
                }
                keySet.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void dispatch(SelectionKey sk) {
        Runnable run = (Runnable) sk.attachment();
        if (run != null) {
            run.run();
        }
    }

    class AcceptorHandler implements Runnable {

        @Override
        public void run() {
            try {
                SocketChannel accept = socketChannel.accept();
                logger.info("接收到一个链接。。。");
                if (accept != null) {

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
