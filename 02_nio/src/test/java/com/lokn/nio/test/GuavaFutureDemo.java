package com.lokn.nio.test;

import com.google.common.util.concurrent.*;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @description:
 * @author: lokn
 * @date: 2022/05/26 21:03
 */
public class GuavaFutureDemo {

//    private static final Logger logger = LoggerFactory.getLogger(GuavaFutureDemo.class);

    private static final int SLEEP_GAP = 100;

    /**
     *  烧开水
     */
    static class HotWaterJob implements Callable<Boolean> {

        @Override
        public Boolean call() throws Exception {
            try {
                System.out.println("洗水壶。。。");
                System.out.println("灌入清水。。。");
                System.out.println("开始烧开水。。。");
                Thread.sleep(SLEEP_GAP);
                System.out.println("水开了。");
            } catch (InterruptedException e) {
                System.err.println("烧热水发生了异常，烧水失败。");
                return false;
            }
            System.out.println("烧水完成！");
            return true;
        }
    }

    static class WashJob implements Callable<Boolean> {

        @Override
        public Boolean call() throws Exception {
            try {
                System.out.println("洗茶壶。。。");
                System.out.println("洗茶杯...");
                System.out.println("拿茶叶...");
                Thread.sleep(SLEEP_GAP);
                System.out.println("清洗完成！");
            } catch (InterruptedException e) {
                System.err.println("清洗过程发生异常。。。");
                return false;
            }
            return true;
        }
    }

    static class MainJob implements Runnable {

        boolean washOk = false;
        boolean cupOk = false;
        int gap = SLEEP_GAP / 10;

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(gap);
                    System.out.println("读书中。。。");
                } catch (InterruptedException e) {
                    System.err.println(Thread.currentThread().getName() + " 发生中断了...");
                }
                if (washOk && cupOk) {
                    drinkTea(washOk, cupOk);
                }
            }
        }

        public void drinkTea(boolean washOk, boolean cupOk) {
            if (washOk && cupOk) {
                System.out.println("泡茶喝，茶喝完！");
                this.washOk = false;
                this.gap = SLEEP_GAP * 100;
            } else if (!washOk) {
                System.out.println("烧水失败，没有茶喝了！");
            } else if (!cupOk) {
                System.out.println("被子洗不了，没有茶喝了！");
            }
        }
    }

    public static void main(String[] args) {
        MainJob mainJob = new MainJob();
        Thread thread = new Thread(mainJob);
        thread.setName("主线程");
        thread.start();

        HotWaterJob hotWaterJob = new HotWaterJob();
        WashJob washJob = new WashJob();

        ExecutorService jPool = Executors.newFixedThreadPool(4);
        ListeningExecutorService servicePool = MoreExecutors.listeningDecorator(jPool);
        ListenableFuture<Boolean> hotFuture = servicePool.submit(hotWaterJob);
        Futures.addCallback(hotFuture, new FutureCallback<Boolean>() {
            @Override
            public void onSuccess(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    mainJob.cupOk = aBoolean;
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                System.out.println("....烧水失败，没有茶喝了");
            }
        }, jPool);

        ListenableFuture<Boolean> washFuture = servicePool.submit(washJob);
        Futures.addCallback(washFuture, new FutureCallback<Boolean>() {
            @Override
            public void onSuccess(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    mainJob.washOk = aBoolean;
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                System.out.println("...杯子洗不了，没有茶喝了");
            }
        }, jPool);

    }

}

