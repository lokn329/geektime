package com.lokn.nio.test.json;

import com.alibaba.fastjson.JSON;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.string.StringEncoder;

import java.nio.charset.StandardCharsets;

/**
 * @description:
 * @author: lokn
 * @date: 2022/07/02 15:50
 */
public class JsonClient {

    private static final String host = "localhost";
    private static final int port = 8089;

    Bootstrap bootstrap = new Bootstrap();

    public static void main(String[] args) {
        new JsonClient().runClient();
    }

    public void runClient() {
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            bootstrap.group(group);
            bootstrap.channel(NioSocketChannel.class);
            bootstrap.option(ChannelOption.ALLOCATOR, ByteBufAllocator.DEFAULT);
            bootstrap.remoteAddress(host, port);
            bootstrap.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    socketChannel.pipeline().addLast(new LengthFieldPrepender(4));
                    socketChannel.pipeline().addLast(new StringEncoder(StandardCharsets.UTF_8));
                }
            });
            ChannelFuture connect = bootstrap.connect();
            connect.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (future.isSuccess()) {
                        System.out.println("jsonClient 连接服务端成功！");
                    } else {
                        System.out.println("jsonClient 连接服务端失败！");
                    }
                }
            });
//            connect.sync();

            Channel channel = connect.channel();
            for (int i = 0; i < 10; i++) {
                JsonMsg jsonMsg = new JsonMsg();
                jsonMsg.setId(i);
                jsonMsg.setContent(i + "->我是文本内容呀！");
                String s = JSON.toJSONString(jsonMsg);
                channel.writeAndFlush(s);
                System.out.println("发送的报文为：" + s);
            }
            channel.flush();
            // 等待通道关闭的异步任务结束
            // 服务监听通道会一直等待通道关闭的异步任务结束
            channel.closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }
    }

}
