package com.lokn.nio.test.json;

import com.alibaba.fastjson.JSONObject;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @description:
 * @author: lokn
 * @date: 2022/07/02 15:28
 */
public class JsonMsgDecoder extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("------");
        String json = (String) msg;
        JsonMsg jsonMsg = JSONObject.parseObject(json, JsonMsg.class);
        System.out.println("JsonMsgDecoder 接收到的msg为：" + jsonMsg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println(cause);
        super.exceptionCaught(ctx, cause);
    }
}
