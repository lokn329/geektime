package com.lokn.nio.test.conc;

import java.util.concurrent.Semaphore;

/**
 * @description:
 * @author: lokn
 * @date: 2022/06/19 23:36
 */
public class SemaphoreTest {

    public static void main(String[] args) {
        int N = 8;
        Semaphore semaphore = new Semaphore(2);
        for (int i = 0; i < N; i++) {
            new Task(semaphore, i).start();
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("主线程结束！");
    }

    static class Task extends Thread {

        private Semaphore semaphore;
        private int n;

        public Task(Semaphore semaphore, int n) {
            this.semaphore = semaphore;
            this.n = n;
        }

        @Override
        public void run() {
            try {
                // 获取资源
                semaphore.acquire();
                Thread.sleep(1000);
                System.out.println(String.format("线程【%s】- 我是第【%d】位工人",
                        Thread.currentThread().getName(), n));
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                semaphore.release();
            }
        }
    }

}
