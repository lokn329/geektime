package com.lokn.nio;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @description:
 * @author: lokn
 * @date: 2022/04/18 23:43
 */
public class FixedLengthFrameDecoderTest {

    @Test
    public void testFrameDecoder() {
        ByteBuf buf = Unpooled.buffer();
        for (int i = 0; i < 9; i++) {
            buf.writeByte(i);
        }
        ByteBuf input = buf.duplicate();
        EmbeddedChannel embeddedChannel = new EmbeddedChannel(new FixedLengthFrameDecoder(3));
        assertTrue(embeddedChannel.writeInbound(input.retain()));
        assertTrue(embeddedChannel.finish());

        ByteBuf read = (ByteBuf) embeddedChannel.readInbound();
        assertEquals(buf.readSlice(3), read);
        read.release();

    }

}
