package com.lokn.gateway;

import com.lokn.gateway.inbound.HttpInboundServer;

import java.util.Arrays;

/**
 * @description:
 * @author: lokn
 * @date: 2022/05/09 00:01
 */
public class NettyServerApplication {

    private static final String GATEWAY_NAME = "NIOGateway";
    private static final String GATEWAY_VERSION = "3.0";

    public static void main(String[] args) {
        String proxyPort = System.getProperty("proxyPort", "8090");
        String proxyServer = System.getProperty("proxyServer", "http://localhost:8081,http://localhost:8082");

        System.out.println(GATEWAY_NAME + " " + GATEWAY_VERSION + " starting...");
        int port = Integer.parseInt(proxyPort);
        HttpInboundServer server = new HttpInboundServer(port, Arrays.asList(proxyServer.split(",")));
        System.out.println(GATEWAY_NAME + " " + GATEWAY_VERSION + " started at http://localhost:" + port + " for server:" + server.toString());

        try {
            server.run();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
