package com.lokn.gateway.filter;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * @description:
 * @author: lokn
 * @date: 2022/05/08 20:34
 */
public class HeaderHttpRequestFilter implements HttpRequestFilter{


    @Override
    public void filter(FullHttpRequest request, ChannelHandlerContext ctx) {
        request.headers().set("mao", "soul");
    }
}
