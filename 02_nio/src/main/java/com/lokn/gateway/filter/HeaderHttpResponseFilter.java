package com.lokn.gateway.filter;

import io.netty.handler.codec.http.FullHttpResponse;

/**
 * @description:
 * @author: lokn
 * @date: 2022/05/08 20:36
 */
public class HeaderHttpResponseFilter implements HttpResponseFilter{
    @Override
    public void filter(FullHttpResponse response) {
        response.headers().set("kk", "java-gateway-2");
        System.out.println("Gateway response filter ");
    }
}
