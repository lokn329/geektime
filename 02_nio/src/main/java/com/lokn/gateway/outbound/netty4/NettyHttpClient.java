package com.lokn.gateway.outbound.netty4;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpRequestEncoder;
import io.netty.handler.codec.http.HttpResponseDecoder;

/**
 * @description:
 * @author: lokn
 * @date: 2022/05/10 21:13
 */
public class NettyHttpClient {

    public void connect(String host, int port) throws InterruptedException {
        NioEventLoopGroup workGroup = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(workGroup);
            b.channel(NioSocketChannel.class);
            b.option(ChannelOption.SO_KEEPALIVE, true);
            b.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    ChannelPipeline pipeline = socketChannel.pipeline();
                    pipeline.addLast(new HttpResponseDecoder())
                            .addLast(new HttpRequestEncoder())
                            .addLast(new NettyHttpClientOutboundHandler());
                }
            });
            ChannelFuture sync = b.connect(host, port).sync();
            sync.channel().write("");
            sync.channel().flush();
            sync.channel().closeFuture().sync();
        } finally {
            workGroup.shutdownGracefully();
        }

    }

}
