package com.lokn.gateway.router;

import java.util.List;
import java.util.Random;

/**
 * @description:
 * @author: lokn
 * @date: 2022/05/08 20:39
 */
public class RandomHttpEndpointRouter implements HttpEndpointRouter {

    @Override
    public String route(List<String> endpoints) {
        int size = endpoints.size();
        Random random = new Random(System.currentTimeMillis());
        return endpoints.get(random.nextInt(size));
    }
}
