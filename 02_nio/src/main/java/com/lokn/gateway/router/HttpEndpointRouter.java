package com.lokn.gateway.router;

import java.util.List;

/**
 *  http终端路由
 * @author lokn
 */
public interface HttpEndpointRouter {

    /**
     * 用于对接请求进行路由，从而实现负载均衡
     * 
     * @param endpoints 终端地址
     * @return
     */
    String route(List<String> endpoints);

    // Load Balance
    // Random
    // RoundRibbon
    // Weight
    // - server01,20
    // - server02,30
    // - server03,50

}
