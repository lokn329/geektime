package com.lokn.echo;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetSocketAddress;

/**
 * @description:
 * @author: lokn
 * @date: 2022/03/28 23:40
 */
public class EchoServer {

    private final int port;

    public EchoServer(int port) {
        this.port = port;
    }

    public static void main(String[] args) throws InterruptedException {
//        if (args.length != 1) {
//            System.err.println("Usage: " + EchoServer.class.getSimpleName() + " <port>");
//        }
//        int port = Integer.parseInt(args[0]);
        int port = 8081;
        EchoServer echoServer = new EchoServer(port);
        while (true) {
            echoServer.start();
        }
    }

    public void start() throws InterruptedException {
        final EchoServerHandler serverHandler = new EchoServerHandler();
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(group)
                // 指定所使用的 NIO 传输 Channel
                .channel(NioServerSocketChannel.class)
                .localAddress(new InetSocketAddress(port))
                // 添加一个EchoServerHandler 到子 Channel 的 ChannelPipeline
                .childHandler(new ChannelInitializer<SocketChannel>(){
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        // EchoServerHandler 被标注为@Shareale，所以我们可以总是使用同样的实例
                        ch.pipeline().addLast(serverHandler);
                    }
                });
            // 异步地绑定服务器，调用sync()方法阻塞等待直到绑定完成
            ChannelFuture sync = b.bind().sync();
            // 获取Channel 的 CloseFuture，并且阻塞当前线程直到它完成
            sync.channel().closeFuture().sync();
        } finally {
            // 关闭 EventLoopGroup 释放所有资源
            group.shutdownGracefully().sync();
        }
    }

}
