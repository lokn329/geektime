package com.lokn.discard;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

/**
 * @description:
 * @author: lokn
 * @date: 2022/05/28 11:18
 */
public class NettyDiscardServer {

    public static void main(String[] args) {
        NettyDiscardServer server = new NettyDiscardServer();
        server.runServer();
    }

    public void runServer() {
        NioEventLoopGroup bossGroup = new NioEventLoopGroup(1);
        NioEventLoopGroup workGroup = new NioEventLoopGroup();

        ServerBootstrap b = new ServerBootstrap();
        // 设置反应器线程组
        b.group(bossGroup, workGroup);
        // 设置通道类型
        b.channel(NioServerSocketChannel.class);
        b.localAddress(80);
        // 设置通道参数
        b.option(ChannelOption.SO_KEEPALIVE, true);
        b.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
        // 装配子通道流水线
        b.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel sc) throws Exception {
                sc.pipeline().addLast(new NettyDiscardHandler());
            }
        });
        try {
            ChannelFuture sync = b.bind().sync();
            System.out.println("服务启动成功，监听端口为：80");
            ChannelFuture channelFuture = sync.channel().closeFuture();
            channelFuture.sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            workGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }

    }

    private void test() throws InterruptedException {
        // 创建服务端启动器
        ServerBootstrap b = new ServerBootstrap();
        // 创建反应器线程组
        NioEventLoopGroup boosGroup = new NioEventLoopGroup(1);
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            // 设置反应器线程组
            b.group(boosGroup, workerGroup);
            // 设置通道类型
            b.channel(NioServerSocketChannel.class);
            // 设置监听端口
            b.bind(new InetSocketAddress(80));
            // 设置通道类型
            b.option(ChannelOption.SO_KEEPALIVE, true);
            b.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
            b.childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel sc) throws Exception {

                }
            });
            // 开始绑定端口，阻塞直到绑定成功
            ChannelFuture future = b.bind().sync();
            SocketAddress socketAddress = future.channel().localAddress();
            future.channel().closeFuture().sync();
        } finally {
            boosGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }

    }

}
