package com.lokn.protobuf;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;

/**
 * @description:
 * @author: lokn
 * @date: 2022/07/04 23:21
 */
public class ProtobufClient {

    private static final int port = 8189;
    private static final String host = "localhost";
    Bootstrap b = new Bootstrap();

    public static void main(String[] args) {
        new ProtobufClient().runClient();
    }

    public void runClient() {
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            b.group(group);
            b.channel(NioSocketChannel.class);
            b.option(ChannelOption.SO_KEEPALIVE, true);
            b.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
            b.remoteAddress(host, port);
            b.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    socketChannel.pipeline().addLast(new ProtobufVarint32LengthFieldPrepender());
                    socketChannel.pipeline().addLast(new ProtobufEncoder());
                }
            });
            ChannelFuture connect = b.connect();
            connect.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (future.isSuccess()) {
                        System.out.println("protobufClient 连接服务端成功！");
                    } else {
                        System.out.println("protobufClient 连接服务端失败！");
                    }
                }
            });
            ChannelFuture sync = connect.sync();
            Channel channel = connect.channel();
            for (int i = 0; i < 10; i++) {
                MsgProto.Msg.Builder builder = MsgProto.Msg.newBuilder();
                builder.setId(i);
                builder.setContent("我是测试啊=" + i);
                builder.build();
                channel.writeAndFlush(builder    );
                System.out.println("报文发送次数：" + i);
            }
            channel.flush();
            channel.closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }

    }

}
