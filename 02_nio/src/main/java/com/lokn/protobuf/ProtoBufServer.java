package com.lokn.protobuf;


import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;

/**
 * @description:
 * @author: lokn
 * @date: 2022/07/04 23:04
 */
public class ProtoBufServer {

    private static final int port = 8189;

    ServerBootstrap b = new ServerBootstrap();

    public static void main(String[] args) {
        new ProtoBufServer().runServer();
    }

    public void runServer() {
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workGroup = new NioEventLoopGroup(8);
        try {
            b.group(bossGroup, workGroup);
            b.channel(NioServerSocketChannel.class);
            b.option(ChannelOption.SO_KEEPALIVE, true);
            b.localAddress(port);
            b.childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    socketChannel.pipeline().addLast(new ProtobufVarint32FrameDecoder());
                    socketChannel.pipeline().addLast(new ProtobufDecoder(MsgProto.Msg.getDefaultInstance()));
                    socketChannel.pipeline().addLast(new ProtoBufBusinessHandler());
                }
            });
            ChannelFuture bind = b.bind();
            ChannelFuture sync = bind.sync();

            System.out.println("protobufServer 启动成功，地址为：" + bind.channel().localAddress());
            bind.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            workGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    static class ProtoBufBusinessHandler extends ChannelInboundHandlerAdapter {

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            MsgProto.Msg protoMsg = (MsgProto.Msg) msg;
            System.out.println("channelRead get msg id = " + protoMsg.getId());
            System.out.println("channelRead get msg content = " + protoMsg.getContent());
        }
    }


}
