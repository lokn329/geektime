package com.lokn.protobuf;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @description:
 * @author: lokn
 * @date: 2022/07/03 16:23
 */
public class ProtobufDemo {

    @Test
    public void test() throws IOException {
        MsgProto.Msg msg = buildMsg();
        byte[] bytes = msg.toByteArray();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(bytes);
        byte[] bytes1 = byteArrayOutputStream.toByteArray();
        MsgProto.Msg msg1 = MsgProto.Msg.parseFrom(bytes1);
        System.out.println(msg1.getId());
        System.out.println(msg1.getContent());
    }

    @Test
    public void test1() throws IOException {
        MsgProto.Msg msg = buildMsg();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(msg.toByteArray());
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        MsgProto.Msg msg1 = MsgProto.Msg.parseFrom(inputStream);
        System.out.println(msg1.getId());
        System.out.println(msg1.getContent());
    }

    /**
     *  解决粘包问题
     * @throws IOException
     */
    @Test
    public void test2() throws IOException {
        MsgProto.Msg msg = buildMsg();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        msg.writeDelimitedTo(outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        MsgProto.Msg msg1 = MsgProto.Msg.parseDelimitedFrom(inputStream);
        System.out.println(msg1.getId());
        System.out.println(msg1.getContent());
    }

    public static MsgProto.Msg buildMsg() {
        MsgProto.Msg.Builder builder = MsgProto.Msg.newBuilder();
        builder.setId(1);
        builder.setContent("我是protobuf协议content");
        MsgProto.Msg build = builder.build();
        return build;
    }

}
