package com.lokn.nio;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * @description:
 * @author: lokn
 * @date: 2022/03/27 16:25
 */
public class HttpClientTest {

    public static void main(String[] args) {
        String url = "http://localhost:8081";
        httpGet(url);
    }

    private static void httpGet(String url) {
        CloseableHttpClient build = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response = null;
        try {
            response = build.execute(httpGet);
            HttpEntity entity = response.getEntity();
            StatusLine statusLine = response.getStatusLine();
            System.out.println("接口响应状态 statusCode = " + statusLine.getStatusCode());
            System.out.println("接口返回结果为：" + EntityUtils.toString(entity));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (build != null) {
                try {
                    build.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
