package com.lokn.nio;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @description:
 * @author: lokn
 * @date: 2022/03/20 23:49
 */
public class HttpServer03 {

    public static void main(String[] args) throws IOException {
        ServerSocket socket = new ServerSocket(8083);
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        while (true) {
            Socket accept = socket.accept();
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    server(accept);
                }
            });
        }
    }

    private static void server(Socket socket)  {
        try {
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println("HTTP/1.1 200 OK");
            writer.println("Content-Type:text/html;charset=utf-8");
            String body = "hell nio-03";
            writer.println("Content-Length:" + body.getBytes().length);
            writer.println();
            writer.write(body);
            writer.close();
            socket.close();
        } catch (Exception e) {

        }
    }

}
