package com.lokn.nio;

import okhttp3.*;

import java.io.IOException;


/**
 * @description:
 * @author: lokn
 * @date: 2022/03/27 16:44
 */
public class OkHttpTest {

    public static void main(String[] args) {
        String url = "http://localhost:8081";
        httpGet(url);
    }

    private static void httpGet(String url) {
        OkHttpClient okHttpClient = new OkHttpClient();
        okhttp3.Request build = new Request.Builder().url(url).build();
        Call call = okHttpClient.newCall(build);
        try {
            Response execute = call.execute();
            ResponseBody body = execute.body();
            System.out.println("响应信息：" + execute.toString());
            System.out.println("响应结果：" + body.string());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
