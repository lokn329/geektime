package com.lokn.nio;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @description:
 * @author: lokn
 * @date: 2022/03/20 23:43
 */
public class HttpServer02 {

    public static void main(String[] args) throws IOException {
        final ServerSocket socket = new ServerSocket(8082);
        while (true) {
            final Socket accept = socket.accept();
            new Thread(() -> {
                server(accept);
            }).start();
        }
    }

    private static void server(final Socket socket) {
        try {
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println("HTTP/1.1 200 OK");
            writer.println("Content-Type:text/html;charset=utf-8");
            String body = "hello nio-02";
            writer.println("Content-Length:" + body.getBytes().length);
            writer.println();
            writer.write(body);
            writer.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
