package com.lokn.nio;

import jdk.net.Sockets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @description:
 * @author: lokn
 * @date: 2022/03/20 23:26
 */
public class HttpServer01 {

    public static void main(String[] args) {
        try {
            ServerSocket socket = new ServerSocket(8081);
            while (true) {
                final Socket accept = socket.accept();
                service(accept);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void service(Socket socket) throws IOException {
        final PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
        printWriter.println("HTTP/1.1 200 OK");
        printWriter.println("Content-Type:text/html;charset=utf-8");
        String body = "hello, nio-01";
        printWriter.println("Content-Length:" + body.getBytes().length);
        printWriter.println();
        printWriter.write(body);
        printWriter.close();
        socket.close();
    }

}
